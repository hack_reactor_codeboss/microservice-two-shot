import json
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import BinVO, Shoe


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"]

class shoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "size",
        "year",
        "brand",
        "model_type",
        "color", 
        "bin", 
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET"])
def api_list_binVOs(request):
  print("list bins called")

  bins = BinVO.objects.all()
  print("binVOs are: ",bins)
  return JsonResponse(
    {"bins": bins},
    encoder=BinVODetailEncoder,
  )


@require_http_methods(["GET", "POST", "DELETE"])
def api_list_shoes(request, shoe_id=None):

    if request.method == "GET":
        if shoe_id is not None:
            shoes = Shoe.objects.filter(id=shoe_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=shoeDetailEncoder,
        )
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=shoe_id)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=shoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        content = json.loads(request.body)

        # Get the ... object and put it in the content dict
        try:
            bin_href = content["bin"]
            bin_vo =BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin_vo
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=shoeDetailEncoder,
            safe=False,
        )
    

def api_show_shoe(request, pk):
    """
    Returns the details for the shoe model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified shoe instance.
    """
    shoe = Shoe.objects.get(id=pk)
    return JsonResponse(
        shoe,
        encoder=shoeDetailEncoder,
        safe=False,
    )
