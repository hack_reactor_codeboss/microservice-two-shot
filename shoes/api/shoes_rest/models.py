from django.db import models
# from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse

# Create your models here.

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    import_id = models.IntegerField(null=True)

    # bin_number = models.PositiveSmallIntegerField()
    # bin_size = models.PositiveSmallIntegerField()
    
    def __str__(self):
        return self.closet_name

class Shoe(models.Model):
    size = models.TextField()
    year = models.TextField()
    brand = models.TextField()
    model_type = models.CharField(max_length=200, null=True, blank=True)
    color = models.TextField()
    # try, for fun? models.TextChoices()
    bin = models.ForeignKey(
        BinVO,
        related_name = "shoes",
        # at what point is ^ code called???
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.brand

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})
