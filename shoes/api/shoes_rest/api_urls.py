from django.urls import path

from .api_views import api_list_shoes, api_show_shoe, api_list_binVOs

urlpatterns = [
    path("bins/", api_list_binVOs, name="api_list_binVOs"),
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:shoe_id>/", api_list_shoes, name="api_delete_shoe"),
]
