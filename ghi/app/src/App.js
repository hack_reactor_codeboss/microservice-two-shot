import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './Components/MainPage.js';
import Nav from './Components/Nav';
import Hats from './Components/Hats.tsx'
import Shoes from './Components/Shoes.js';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats/" element={<Hats />}/>
          <Route path="/shoes/" element={<Shoes />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
