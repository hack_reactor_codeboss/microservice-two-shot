import React from "react";
import { Location } from "./Hats";

interface Props {
  category: string;
  label?: string;
  value: string;
  options: Location[];
  onSelect: (category: string, choice: string) => void;
}

const LocationFormField = ({
  category,
  label,
  value,
  options,
  onSelect,
}: Props) => {


  if (!label) {
    label = category.replace(/^./, (l) => l.toUpperCase());
  }

  return (
    <div className="mt-3">
      <div className="input-group">
      <label className="input-group-text" htmlFor={category}>
        {label}
      </label>
      <select
        className="form-select"
        id={category}
        value={value}
        onChange={(event) => onSelect(category, event.target.value)}
      >
        <option value="">Choose...</option>
        {options.map((option: Location) => (
          <option key={option.import_id} value={option.import_href}>
            {option.name}
          </option>
        ))}
      </select>

      </div>
    </div>
  );
};

export default LocationFormField;
