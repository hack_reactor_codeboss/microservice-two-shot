import React, { useState } from "react";
import ItemRow from "./ItemRow.tsx";
import { Hat } from "./Hats.tsx";
import Filter from "./Filter.tsx";
import ConfirmDeleteModal from "./ConfirmDeleteModal.tsx";

interface Props {
  items: Hat[];
  onDelete: (id: number) => void;
}

const ListItems = ({ items, onDelete }: Props) => {
  const emptyFilter = {
    color: "",
    fabric: "",
    name: "",
    location: "",
  };

  const [filter, setFilter] = useState(emptyFilter);
  const { color, fabric, name, location } = filter;

  const FilterCategory = (category: string, choice: string) => {
    setFilter({ ...filter, [category]: choice });
  };

  const FilterLocations = items
    .map((hat) => hat.location.name)
    .filter((v, i, a) => a.indexOf(v) === i);

  let visibleItems = items;

  for (const prop in filter) {
    if (filter[prop]) {
      if (prop === "location") {
        visibleItems = visibleItems.filter(
          (hat) => hat.location.name === filter[prop]
        );
      } else {
        visibleItems = visibleItems.filter((hat) => hat[prop] === filter[prop]);
      }
    }
  }

  const [itemToDelete, setItemToDelete] = useState<number>(-1);

  return (
    <>
      <table className="table align-middle table-bordered table-striped mt-5">
        <thead>
          <tr>
            <th>
              <button
                className="btn btn-secondary"
                onClick={() => setFilter(emptyFilter)}
              >
                Reset Filters
              </button>
            </th>
            <th>
              <Filter
                category="color"
                value={color}
                hats={items}
                onSelect={FilterCategory}
              />
            </th>
            <th>
              <Filter
                category="fabric"
                value={fabric}
                hats={items}
                onSelect={FilterCategory}
              />
            </th>
            <th>
              <Filter
                category="name"
                value={name}
                hats={items}
                onSelect={FilterCategory}
              />
            </th>
            <th>
              <Filter
                category="location"
                value={location}
                choices={FilterLocations}
                onSelect={FilterCategory}
              />
            </th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {visibleItems.map((item) => (
            <ItemRow
              key={item.id}
              deleteButtonClick={() => setItemToDelete(item.id)}
              item={item}
            />
          ))}
        </tbody>
      </table>
      <ConfirmDeleteModal onDelete={onDelete} item={itemToDelete} />
    </>
  );
};

export default ListItems;
