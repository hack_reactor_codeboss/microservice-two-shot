export const list_hats_url = "http://localhost:8090/api/hats/"
export const new_hat_url = "http://localhost:8090/api/hats/"
export const list_locations_url = "http://localhost:8090/api/locations/"
export const delete_hat_url = (id) => `http://localhost:8090/api/hats/${id}/`
