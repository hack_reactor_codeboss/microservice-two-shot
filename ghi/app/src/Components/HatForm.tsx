import React, { FormEvent, useEffect, useState } from "react";
import { Location } from "./Hats.tsx";
import FormTextField from "./FormTextField.tsx";
import { list_locations_url, new_hat_url } from "./urls.js";
import LocationFormField from "./LocationFormField.tsx";




const HatForm = ({ fetchPK }) => {
  const [locations, setLocations] = useState<Location[]>([]);
  const [ goFetch, setFetch ] = fetchPK

  const getLocations = async () => {
    const response = await fetch(list_locations_url);
    if (response.ok) {
      const { locations } = await response.json();
      setLocations(locations);
    } else {
      console.error("An error has occured fetching locations");
    }
  };

  useEffect(() => {
    getLocations();
  }, []);

  const emptyForm = {
    color: "",
    fabric: "",
    name: "",
    picture_url: "",
    location: "",
  };

  const [formData, setFormData] = useState(emptyForm);

  const { color, fabric, name, picture_url, location } = formData;

  const onSelect = (category: string, choice: string) => {
    setFormData({ ...formData, [category]: choice });
  };

  const onSubmit = async (event: FormEvent) => {
    event.preventDefault();
    const fetchOpts = {
      method: "post",
      body: JSON.stringify(formData)
    }
    const response = await fetch(new_hat_url,fetchOpts)
    if (response.ok){
      setFetch(!goFetch)
      setFormData(emptyForm)
    }
  };

  return (
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="staticBackdropLabel">
            Create A New Hat:
          </h5>
          <button
            onClick={() => setFormData(emptyForm)}
            type="button"
            className="btn btn-second btn-close"
            data-bs-dismiss="modal"
            aria-label="Close"
          ></button>
        </div>
        <div className="modal-body">
          <form onSubmit={onSubmit}>
            <div className="mx-5">
              <FormTextField
                category="color"
                value={color}
                onSelect={onSelect}
                large={true}
              />
              <FormTextField
                category="fabric"
                value={fabric}
                onSelect={onSelect}
                large={true}
              />
              <FormTextField
                category="name"
                label="Style"
                value={name}
                onSelect={onSelect}
                large={true}
              />
              <FormTextField
                category="picture_url"
                label="Picture URL"
                value={picture_url}
                placeholder="www.image-url.com/image/"
                onSelect={onSelect}
              />
              <LocationFormField
                category="location"
                value={location}
                options={locations}
                onSelect={onSelect}
              />
            </div>

            <div className="modal-footer mt-3">
              <div className="d-flex flex-row-reverse">
                <button
                  className="btn btn-primary ml-3"
                  data-bs-dismiss="modal"
                >
                  New Hat!
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default HatForm;
