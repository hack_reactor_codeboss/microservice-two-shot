import React from "react";

interface Props {
  onDelete: (item: number) => void;
  item: number;
}

const confirmDeleteModal = ({ onDelete, item }: Props) => {
  return (
    <div className="modal fade" id="confirmDeleteModal" tabIndex={-1}>
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-body">
            <div className="d-flex justify-content-between">
              <h5 className="modal-title" id="exampleModalLabel">
                Are You Sure?
              </h5>
              <div>
                <button
                  className="btn btn-secondary mx-3"
                  data-bs-dismiss="modal"
                >
                  Cancel
                </button>
                <button
                  onClick={() => {
                    onDelete(item);
                  }}
                  className="btn btn-outline-danger"
                  data-bs-dismiss="modal"
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default confirmDeleteModal;
