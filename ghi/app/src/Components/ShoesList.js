import { useState, useEffect} from 'react';

function ShoesList() {
  const [Shoes, setShoes] = useState([])

  const getData = async ()=> {
    const response = await fetch('http://localhost:8001/api/Shoes/');
    if (response.ok) {
      const { Shoes } = await response.json();
      setShoes(Shoes);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  useEffect(()=> {
    getData()
  }, []);

  return (
    <div className="my-5 container">
      <div className="row">
        <h1>Current Shoes</h1>

        <table className="table table-striped m-3">
          <thead>
            <tr>
              <th>Name</th>
              <th>Bin</th>
            </tr>
          </thead>
          <tbody>
            {Shoes.map(Shoe => {
              return (
                <tr key={Shoe.href}>
                  <td>{ Shoe.name }</td>
                  <td>{ Shoe.id }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default ShoesList;
