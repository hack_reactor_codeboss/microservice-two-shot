import React from "react";
import { Hat } from "./Hats";

interface Props {
  category: string;
  choices?: string[];
  value: string;
  hats?: Hat[];
  onSelect: (category: string, event: string) => void;
}

const Filter = ({ category, choices, value, hats = [], onSelect } : Props) => {

  const options = choices ?
   choices :
   hats.map(hat => hat[category]).filter((v,i,a)=> a.indexOf(v) === i)

  const label = category.replace(/^./,(l)=> l.toUpperCase())
  
  return (
    <select
      className="form-select"
      onChange={(event) => onSelect(category, event.target.value)}
      value={value}
    >
      <option value="" key="">
        {label}s...
      </option>
      {options.map((choice: string) => (
        <option key={choice} value={choice}>
          {choice}
        </option>
      ))}
    </select>
  );
};

export default Filter;
