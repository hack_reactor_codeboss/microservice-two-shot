import React, { useEffect, useState } from "react";
import ListItems from "./ListItems.tsx";
import { delete_hat_url, list_hats_url } from "./urls.js";
import HatFormModal from "./HatFormModal.tsx";

export interface Location {
  name: string;
  import_href: string;
  import_id: number;
}

export interface Hat {
  id: number;
  name: string;
  fabric: string;
  color: string;
  picture_url: string;
  location: Location;
}

const Hats = () => {
  const [hats, setHats] = useState<Hat[]>([]);
  const fetchPK = useState(false);
  const [goFetch, setFetch] = fetchPK;

  const onDelete = async (id: number) => {
    const saved_hats = [...hats];
    setHats([...hats.filter((hat) => hat.id !== id)]);

    const url = delete_hat_url(id);
    const response = await fetch(url, { method: "DELETE" });
    if (!response.ok) {
      setHats(saved_hats);
      // create modal that notifies that the hat was not deleted
    }
  };

  const getHats = async () => {
    const response = await fetch(list_hats_url);
    if (response.ok) {
      const { hats } = await response.json();
      setHats(hats);
    } else {
      console.error("An error has occured fetching hats");
    }
  };

  useEffect(() => {
    getHats();
  }, [goFetch]);

  return (
    <>
      <div className="m-5">
        <div>
          <div className="d-flex justify-content-between">
            <div className="d-flex">
              <h1 className="fw-bold">
                <u>Hats!</u>
              </h1>
              <HatFormModal fetchPK={fetchPK} />
            </div>
          </div>
          <ListItems items={hats} onDelete={onDelete} />
        </div>
      </div>
    </>
  );
};

export default Hats;
