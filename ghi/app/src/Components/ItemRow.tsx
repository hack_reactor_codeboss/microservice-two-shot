import React from "react";
import { Hat } from "./Hats";

interface Props {
  item: Hat;
  deleteButtonClick: () => void;
}

const ItemRow = ({ item, deleteButtonClick }: Props) => {
  return (
    <tr>
      <td>
        <img src={item.picture_url} width={100} alt="" className="img-thumbnail" />
      </td>
      <td>{item.color}</td>
      <td>{item.fabric}</td>
      <td>{item.name}</td>
      <td>{item.location.name}</td>
      <td>
        <button
          className="btn btn-outline-danger"
          data-bs-toggle="modal"
          data-bs-target="#confirmDeleteModal"
          onClick={deleteButtonClick}
        >
          Delete
        </button>
      </td>
    </tr>
  );
};

export default ItemRow;
