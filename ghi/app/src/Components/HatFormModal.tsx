import React, { useState } from "react";
import HatForm from "./HatForm.tsx";
import { Hat } from "./Hats.tsx";




const HatFormModal = ({ fetchPK }) => {
  return (
    <>
      <button
        type="button"
        className="btn btn-info btn-lg mx-5"
        data-bs-toggle="modal"
        data-bs-target="#staticBackdrop"
      >
        New Hat
      </button>
        <div
          className="modal fade mt-5"
          id="staticBackdrop"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabIndex={-1}
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <HatForm fetchPK={fetchPK}/>
        </div>

    </>
  );
};

export default HatFormModal;
