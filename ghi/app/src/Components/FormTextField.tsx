import React from "react";

interface Props {
  category: string;
  label?: string;
  large?: boolean;
  placeholder?: string;
  value: string;
  onSelect: (category: string, choice: string) => void;
}

function FormTextField({
  category,
  label,
  large,
  placeholder,
  value,
  onSelect,
}: Props) {


  if (!label) {
    label = category.replace(/^./, (l) => l.toUpperCase());
  }

  return (
    <>
      <div
        className={
          large ? "input-group input-group-lg my-3" : "input-group my-3"
        }
      >
        <span className="input-group-text">{label}</span>
        <input
          id={category}
          value={value}
          placeholder={placeholder ? placeholder : ""}
          onChange={(event) => {
            onSelect(category, event.target.value);
          }}
          type="text"
          className="form-control"
          aria-label="Sizing example input"
          aria-describedby="inputGroup-sizing-default"
        />
      </div>
    </>
  );
}

export default FormTextField;
