import React, {useEffect, useState, FormEvent } from "react"
// import React, {useEffect, useState } from 'react';

function Shoes() {

  const [size, setSize] = useState('');
  const [year, setYear] = useState('');
  const [brand, setBrand] = useState('');
  const [model_type, setModelType] = useState('');
  const [color, setColor] = useState('');
  const [id, setID] = useState('');
  const [bins, setBins] = useState([]);


  const fetchData = async () => {
    const url = 'http://localhost:8080/api/bins/';    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.size = size;
    data.year = year;
    data.brand = brand;
    data.color = color;
    data.model_type = model_type;
    // data.bin = "/api/bins/1";

    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const shoeResponse = await fetch(shoeUrl, fetchOptions);
    if (shoeResponse.ok) {
      setSize('');
      setYear('');
      setBrand('');
      setColor(true);
    }
  }

  const handleChangeSize = (event) => {
    const value = event.target.value;
    setSize(value);
  }
  const handleChangeYear = (event) => {
    const value = event.target.value;
    setYear(value);
  }
  const handleChangeBrand = (event) => {
    const value = event.target.value;
    setBrand(value);
  }
  const handleChangeColor = (event) => {
    const value = event.target.value;
    setColor(value);
  }
  const handleChangeModelType = (event) => {
    const value = event.target.value;
    setModelType(value);
  }
 
  // CSS classes for rendering
  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (bins.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  // if (hasSignedUp) {
  //   messageClasses = 'alert alert-success mb-0';
  //   formClasses = 'd-none';
  // }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          {/* <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="/logo.svg"/> */}
        </div>

        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">Time to get laced up, with Shoes!</h1>
                <p className="mb-3">
                  Please choose from the options below.
                </p>
                <div className={spinnerClasses} id="loading-conference-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                    {/* {bins.map(conference => {
                      return (
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                      )
                    })} */}
                </div>
                <p className="mb-3">Now, can we do for you?</p>
                <div className="row">
                  <div className="col">
                    <div className="form-group range__slider mb-3">
                      <input onChange={handleChangeYear} required placeholder="Year" type="range" step="1" id="year" name="year" className="form-control" />
                      <label htmlFor="year">Year</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeBrand} type="text" id="brand" name="brand" className="form-control" />
                      <label htmlFor="brand">Brand</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeColor} type="text" id="color" name="color" className="form-control" />
                      <label htmlFor="color">Color</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeModelType} type="text" id="modelType" name="modelType" className="form-control" />
                      <label htmlFor="modelType">Model type</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeSize} type="text" id="size" name="size" className="form-control" />
                      <label htmlFor="size">Size</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create Shoe!</button>
              </form>
              <div className={messageClasses} id="success-message">
                Congratulations! You've just created a shoe!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}


export default Shoes;
