# Generated by Django 4.0.3 on 2024-01-31 00:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_locationvo_import_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='locationvo',
            old_name='name',
            new_name='closet_name',
        ),
        migrations.CreateModel(
            name='Hat',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('style_name', models.CharField(max_length=200)),
                ('fabric', models.CharField(max_length=200)),
                ('color', models.CharField(max_length=200)),
                ('picture_url', models.CharField(max_length=200)),
                ('location', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hats', to='hats_rest.locationvo')),
            ],
        ),
    ]
