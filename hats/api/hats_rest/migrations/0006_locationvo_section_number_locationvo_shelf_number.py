# Generated by Django 4.0.3 on 2024-01-31 00:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0005_alter_hat_picture_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationvo',
            name='section_number',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='locationvo',
            name='shelf_number',
            field=models.IntegerField(null=True),
        ),
    ]
