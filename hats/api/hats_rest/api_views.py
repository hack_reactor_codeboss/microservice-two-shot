import json
from django.http import JsonResponse
from django.shortcuts import render
from hats_rest.models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from hats_rest.encoders import HatListEncoder, LocationVOListEncoder
from common.picture_url import stock_picture_url

# Create your views here.

@require_http_methods(["GET"])
def api_list_locationVOs(request):
  print("list locations called")
  """
  generate a list of all locationsVOs
  for options when creating new Hat objects
  {
    "locationsVOs: [
      {
        "name": locations name,
        "href": url to the location
      },
      ...
  }
  """
  locations = LocationVO.objects.all()
  print("locationVOs are: ",locations)
  return JsonResponse(
    {"locations": locations},
    encoder=LocationVOListEncoder,
  )


@require_http_methods(["GET","POST","DELETE"])
def api_list_hats(request,id=None):
  '''
  this api will show all hats and create new hats

  GET->
  returns
  {
    "hats":[
      {hat object},
      ...
    ]
  }
  POST->
  expected json input:
    {
      "style_name": "baseball",
      "fabric": "cotton",
      "color": "blue",
    ? "picture_url": "url_string",
      "loaction": "href to location entity"
    }


  DELETE ->
   delete the hat object
  create new hat and return the details of that hat.
  '''


  if request.method == "GET":
    # GET
    hats = Hat.objects.all()
    return JsonResponse(
      {"hats": hats},
      encoder=HatListEncoder,
    )

  elif  request.method == "DELETE" and id:
    # DELETE
    try:
      hat = Hat.objects.get(id=id)
      hat.delete()
      return JsonResponse(
        hat,
        encoder=HatListEncoder,
        safe=False,
      )
    except Hat.DoesNotExist:
      return JsonResponse(
        {"message": "Does Not Exist"},
        status=404
      )

  else:
    # POST
    content = json.loads(request.body)
    try:
      location = LocationVO.objects.get(import_href=content["location"])
      content["location"] = location
    except LocationVO.DoesNotExist:
      return JsonResponse(
        {"message": "Invalid Location data"},
        status=400,
      )
    if "picture_url" not in content or not content["picture_url"]:
      content["picture_url"] = stock_picture_url

    hat = Hat.objects.create(**content)
    return JsonResponse(
      hat,
      encoder=HatListEncoder,
      safe=False,
    )
