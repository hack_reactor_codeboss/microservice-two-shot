from django.urls import path
from .api_views import (
  api_list_hats,
  api_list_locationVOs,
)


urlpatterns = [
  path("locations/",api_list_locationVOs,name="api_list_locationVOs"),
  path("hats/",api_list_hats,name="api_list_hats"),
  path("hats/<int:id>/",api_list_hats,name="api_delete_hat")
]
