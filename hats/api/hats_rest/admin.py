from django.contrib import admin
from hats_rest.models import LocationVO, Hat


# Register your models here.
@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
	list_display = [
		"name",
		"import_href",
	]


@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
	list_display = [
		"color",
		"name",
		"fabric",
		"picture_url",
		"location",
	]
