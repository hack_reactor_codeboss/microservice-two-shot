from django.db import models

# Create your models here.
class LocationVO(models.Model):
  name = models.CharField(max_length=200)
  import_id = models.IntegerField(null=True)
  import_href = models.CharField(max_length=200, unique=True)


  def __str__(self):
      return self.name



class Hat(models.Model):
  name = models.CharField(max_length=200)
  fabric = models.CharField(max_length=200)
  color = models.CharField(max_length=200)
  picture_url = models.CharField(max_length=200,null=True,blank=True)
  location = models.ForeignKey(
     LocationVO,
     related_name = "hats",
     on_delete=models.CASCADE
    )


  def __str__(self):
      return f'{self.color} {self.style_name} hat'
