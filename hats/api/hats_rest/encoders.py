
from common.json import ModelEncoder
from hats_rest.models import LocationVO, Hat

class LocationVOListEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "name",
        "import_href",
        "import_id"
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "name",
        "fabric",
        "color",
        "picture_url",
        "location",
    ]

    encoders= { "location" : LocationVOListEncoder()}
