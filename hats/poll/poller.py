import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
from hats_rest.models import LocationVO

def poll():
    while True:
        print('Hats poller polling for data')
        try:
            # Write your polling logic, here
            url = "http://wardrobe-api:8000/api/locations/"
            response = requests.get(url)
            content = json.loads(response.content)
            for location in content["locations"]:
                LocationVO.objects.update_or_create(
                    # import_href,import_id,closet_name are the name of the attirbutes on my VO class
                    # "href","id","closet_name" are the key values returned in the json response from wardrobe-api
                    name = location["closet_name"],
                    import_href = location["href"],
                    import_id = location["id"],
                )
        except Exception as e:
            print("Exception thrown, there is a problem")
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
