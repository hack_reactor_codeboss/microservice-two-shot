# Wardrobify

Team:
- Zzmarr Stone - Shoes
- Josh Piasecki - Hats

## Design

### Shoes microservice

-BinVO pointing to Bin data inside of Wardrobe API
-id
-import_href
-Shoes that point to BinVO
-id
-bin
Action/need to do: Insomnia works, couldn't connect front end
GET - list of all bins
GET - get details of 1 bin
POST - create new bin
PUT - updates details of 1 bin
DELETE - deletes 1 bin

### Hats microservice
#### - API
  - all API requests will be made to "... /api/hats/"
  - Supported requests:
    - GET
      - returns a list containing all Hats in the database
      - the list is called "hats"
    - POST
      - creates and returns a new hat in the database
      - fields in the request body will need to be:
        - "name" - a string
        - "fabric" - a string
        - "color" - a string
        - "location" - a string containing the import HREF to the location
        - "picture_url" - optional, a string containing the image url
    - DELETE
      - deletes a hat from the database
      - the id number of the hat to delete will need to be passed at the end of the URL
        - "/api/hats/2/"
      - returns the properties of the deleted hat
